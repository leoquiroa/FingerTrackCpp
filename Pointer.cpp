#include "Pointer.h"
//#include <json/json.h>

////////////////////////////////////////////////////////////////////////////////

class finalMat { 
    public: 
        int indexFrame;
        char typeFrame;
        float posX;
        float posY;
        float radius;
    finalMat(int ixf, char tf, float xx, float yy, float rd){ 
        indexFrame = ixf;
        typeFrame = tf;
        posX = xx;
        posY = yy;
        radius = rd;
    }
    finalMat(){}
}; 

struct Steps
{
    float XX;
    float YY;
};

////////////////////////////////////////////////////////////////////////////////

/*
 * the accumulator
 * dp = 1, same resolution as the original image. 
 * dp = 2, resolution has half as big width and height.
 * 
 * 
 * Minimum distance between the centers of the detected circles.
 * small    -   multiple neighbor circles may be falsely detected
 * big      -   some circles may be missed
 * 
 * It's the accumulator threshold for the circle centers at the detection stage
 * small    -   multiple neighbor circles may be falsely detected
 * big      -   some circles may be missed
 * 
 */

string path = "";
float thdPointer = 0.05;
int minFrame = 2;
int maxFrame = 1;
uint minRadio = 10;
uint maxRadio = 13;
uint dp = 1;
uint minDist = 5;
uint param1 = 10;
uint param2 = 20;
stringstream ssp;
vector<finalMat> finalVec;
Steps steps;
int rangeCrop = 50;
vector<int> ListToFix;
Mat imgPrev, imgCurrent, imgDiff;
vector<Vec3f> circles;

////////////////////////////////////////////////////////////////////////////////

int Pointer::classifyFrames(int totalFrames,string folderName){
    fprintf(stderr, "Begin classifyFrames!\n");
    path = folderName;
    int ixVec = 0;
    maxFrame = totalFrames;
    for(int ixFrame = minFrame; ixFrame < maxFrame; ++ixFrame){
        imgPrev = readImg(ixFrame-1);
        if(!imgPrev.data) return -1;
        imgCurrent = readImg(ixFrame);
        if(!imgCurrent.data) return -1;
        imgDiff = getDiffImg(imgPrev, imgCurrent);
        if(!imgDiff.data) return -1;
        circles = getCircles(imgDiff);
        if(circles.size()>0){//pointer or transition
            if(IsOnlyFinger(imgDiff, thdPointer)){
                finalVec.push_back(finalMat(ixFrame, 'p', -1, -1, -1));
                ClassifyPointer(circles,ixVec);
            }else{
                finalVec.push_back(finalMat(ixFrame, 't', -1, -1, -1));
            }
        }else{//nothing
            finalVec.push_back(finalMat(ixFrame, 'n', -1, -1, -1));
        }
        ixVec++;
    }
    imgPrev.release();
    imgCurrent.release();
    imgDiff.release();
    vector<Vec3f>().swap(circles);
    fprintf(stderr, "End classifyFrames!\n");
}
int Pointer::predictData(){
    fprintf(stderr, "Begin predictData!\n");
    float meanR = avgRadio();
    bool startWithP = false;
    for(vector<int>::size_type ix = 0; ix != finalVec.size(); ix++){
        if(finalVec[ix].typeFrame == 't'){
            if(ix > 0 && finalVec[ix-1].typeFrame == 'p')
                startWithP = true;
            if(startWithP)
                ListToFix.push_back(ix);
        }else{
            if(finalVec[ix].typeFrame == 'p')
                whenBeginAndEnd(ListToFix,ix,meanR);
            else if(finalVec[ix].typeFrame == 'n')
                whenOnlyBegin(ListToFix, meanR);
            ListToFix.clear();
            startWithP = false;
        }
    }
    fprintf(stderr, "End predictData!\n");
}
int Pointer::calculateTransitions(){
    fprintf(stderr, "Begin calculateTransitions!\n");
    vector<Vec3f> circleSobel;
    Mat cropped;
    for(vector<int>::size_type ix = 0; ix != finalVec.size(); ix++){
        if(finalVec[ix].typeFrame == 't' && finalVec[ix].posX > 0 ){
            float xCurrent = finalVec[ix].posX;
            float yCurrent = finalVec[ix].posY;
            cropped = cropImg(ix, xCurrent, yCurrent);
            circleSobel = applySobel(cropped);
            replaceTransitions(ix, xCurrent, yCurrent, circleSobel);
        }
    }
    cropped.release();
    vector<Vec3f>().swap(circleSobel);
    fprintf(stderr, "End calculateTransitions!\n");
}
void Pointer::writeJSON(){
    fprintf(stderr, "Begin writeJSON!\n");
    ofstream myfile;
    myfile.open ((path + ".JSON").c_str());
    myfile << "[\n";
    int limit = finalVec.size()-1;
    for(vector<int>::size_type ix = 0; ix < finalVec.size(); ix++){
        myfile << "\t{\n";
        myfile << "\t\t\"frame\":\""+castValue<int>(finalVec[ix].indexFrame)+"\",\n";
        myfile << "\t\t\"y\":\""+castValue<int>(finalVec[ix].posY)+"\",\n";
        myfile << "\t\t\"x\":\""+castValue<int>(finalVec[ix].posX)+"\"\n";
        if(ix==limit) 
            myfile << "\t}\n";
        else
            myfile << "\t},\n";
    }
    myfile << "]\n";
    myfile.close();
    fprintf(stderr, "End writeJSON!\n");
}
void Pointer::printValues(){
    for(vector<int>::size_type ix = 0; ix != finalVec.size(); ix++){
        cout << 
        finalVec[ix].indexFrame << ',' <<
        (char)finalVec[ix].typeFrame << ',' << 
        finalVec[ix].posX << ',' <<
        finalVec[ix].posY << ',' << 
        finalVec[ix].radius << ',' << 
        endl;
    }
}

////////////////////////////////////////////////////////////////////////////////

void Pointer::replaceTransitions(int ix, float xCurrent, float yCurrent, vector<Vec3f> circles){
    if(circles.size() == 1){
        finalVec[ix].posX = (xCurrent - rangeCrop ) + circles[0][0];
        finalVec[ix].posY = (yCurrent - rangeCrop ) + circles[0][1];
        finalVec[ix].radius = circles[0][2];
    }
}
Mat Pointer::cropImg(int ix, float xCurrent, float yCurrent){
    Mat imgGray = readImg(finalVec[ix].indexFrame);
    int limitH = imgGray.rows;
    int limitW = imgGray.cols;
    int x_bgn = valCropBgn((xCurrent-rangeCrop),limitW);
    int y_bgn = valCropBgn((yCurrent-rangeCrop),limitH);
    int width = valCropEnd(x_bgn, limitW, rangeCrop);
    int height = valCropEnd(y_bgn, limitH, rangeCrop);
    return imgGray(Rect(x_bgn,y_bgn,width,height));
}
int Pointer::valCropEnd(int valBgn, int limit, int rangeCrop){
    int valEnd = valBgn+(rangeCrop*2);
    if(valEnd>limit)
        return rangeCrop - (valEnd - limit);
    else
        return rangeCrop*2;
}
int Pointer::valCropBgn(int value, int limit){
    if(value>limit) return limit;
    else if(value<0) return 0;
    else return value;
}
vector<Vec3f> Pointer::applySobel(Mat cropImg){
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    /// Generate grad_x and grad_y
    Mat grad;
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    /// Gradient X
    Sobel( cropImg, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );
    /// Gradient Y
    Sobel( cropImg, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );
    /// Total Gradient (approximate)
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );
    //
    grad_x.release();
    grad_y.release();
    abs_grad_x.release();
    abs_grad_y.release();
    return getCircles(grad);
}

////////////////////////////////////////////////////////////////////////////////

void Pointer::whenOnlyBegin(vector<int> ListToFix, float meanR){
    if(ListToFix.size() > 0){
        //calculate the average step
        calculateStepForT(ListToFix);
        //fill the holes
        fillHoles(ListToFix, steps.XX, steps.YY, meanR);
    }
}
void Pointer::whenBeginAndEnd(vector<int> ListToFix,int ix,float meanR){
    if(ListToFix.size() > 0){
        int sizeJump = ListToFix.size();
        float stepX = calculateStepForP(ix, sizeJump, 'X');
        float stepY = calculateStepForP(ix, sizeJump, 'Y');
        //fill the holes
        fillHoles(ListToFix, stepX, stepY, meanR);
    }
}
void Pointer::fillHoles(vector<int> ListToFix, float stepX, float stepY, float meanR){
    for(vector<int>::size_type ix = 0; ix != ListToFix.size(); ix++){
        int current = ListToFix[ix];
        finalVec[current].posX = finalVec[current-1].posX + stepX;
        finalVec[current].posY = finalVec[current-1].posY + stepY;
        finalVec[current].radius = meanR;
    }
}
void Pointer::calculateStepForT(vector<int> ListToFix){
    //get real index
    int last = ListToFix[0]-1;
    int cont = 0;
    //begin
    float bgnX = 0.0; 
    float bgnY = 0.0; 
    //end
    float endX = finalVec[last].posX; 
    float endY = finalVec[last].posY;

    //look for the beginning of this block
    for(vector<int>::size_type iixx = last; iixx != 0; iixx--){
        if(finalVec[iixx].typeFrame == 'n'){
            bgnX = finalVec[iixx+1].posX;
            bgnY = finalVec[iixx+1].posY;
            break;
        }
        cont++;
    }
    //calculate the average step
    float stepY = (endY - bgnY)/cont;
    float stepX = (endX - bgnX)/cont;
    //assign
    steps.XX = stepX/ListToFix.size();
    steps.YY = stepY/ListToFix.size();
}
float Pointer::calculateStepForP(int ix, int sizeJump, char coord){
    float Prev = 0.0;
    float Post = 0.0;
    switch(coord){
        case 'X':
            Post = finalVec[ix].posX;
            Prev = finalVec[ix - sizeJump -1].posX;
        break;
        case 'Y':
            Post = finalVec[ix].posY;
            Prev = finalVec[ix - sizeJump -1].posY;
        break;
    }
    float diffX = (Post - Prev);
    return diffX/(float)(sizeJump + 1);
}
float Pointer::avgRadio(){
    float mean = 0.0;
    int cont = 0;
    for(vector<int>::size_type ix = 0; ix != finalVec.size(); ix++){
        float val = finalVec[ix].radius;
        if(val > 0){
            mean += val;
            cont++;
        }
    }
    return mean/(float)cont;
}

////////////////////////////////////////////////////////////////////////////////

void Pointer::setValuePointer(int i, vector<Vec3f> c, int ix){
    finalVec[i].posX = c[ix][0];         //0 - X
    finalVec[i].posY = c[ix][1];         //1 - Y
    finalVec[i].radius = c[ix][2];       //2 - R
}
void Pointer::ClassifyPointer(vector<Vec3f> c, int i){
    int numPoints = c.size();
    if(numPoints == 1){
        setValuePointer(i, c, 0);
    }else if(numPoints == 2){
        //centers of two points
        float center1X = abs(c[0][0] - finalVec[i-1].posX);
        float center1Y = abs(c[0][1] - finalVec[i-1].posY);
        float center2X = abs(c[1][0] - finalVec[i-1].posX);
        float center2Y = abs(c[1][1] - finalVec[i-1].posY);
        //difference of the two points
        float diffX = abs(center1X - center2X);
        float diffY = abs(center1Y - center2Y);
        // higher
        if(diffX > diffY){          //X higher
            if(center1X > center2X){                //0 is higher than 1
                setValuePointer(i, c, 0);
            }else{                                  //1 is higher than 0
                setValuePointer(i, c, 1);
            }
        }else if(diffX < diffY){    //Y higher
            if(center1Y > center2Y){                //0 is higher than 1
                setValuePointer(i, c, 0); 
            }else{                                  //1 is higher than 0
                setValuePointer(i, c, 1);
            }
        }else{                                      //diagonal
            
        }
    }else{                                          //more than 2 points
        finalVec[i].typeFrame = 't';
    }
}
bool Pointer::IsOnlyFinger(Mat img, float thd){
    Mat resThd;
    threshold(img,resThd,10,255,0);        
    float perc = (float)countNonZero(resThd)/
                (float)(img.cols * img.rows);
    resThd.release();
    return (perc<thd) ? true : false ;            
}
vector<Vec3f> Pointer::getCircles(Mat img){
    vector<Vec3f> circles;
    HoughCircles( 
        img, 
        circles, 
        CV_HOUGH_GRADIENT, 
        dp, 
        minDist, 
        param1, 
        param2, 
        minRadio, 
        maxRadio );
    return circles;
}
Mat Pointer::getDiffImg(Mat A, Mat B){
    Mat imgDiff;
    absdiff(A, B, imgDiff);
    return imgDiff;
}
Mat Pointer::readImg(int id){
    string filePrev = path + "/f" + castValue<int>(id) + ".png";
    return imread(filePrev,CV_LOAD_IMAGE_GRAYSCALE);
}

template<class Type1>
string Pointer::castValue(Type1 value){
    string local = "";
    ssp << value;
    local = ssp.str();
    ssp.str("");
    ssp.clear();
    return local;
}

/*
 * Threshold
 * https://docs.opencv.org/2.4/doc/tutorials/imgproc/threshold/threshold.html
 * Hough Transform
 * https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_circle/hough_circle.html
 * Sobel Derivatives
 * https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/sobel_derivatives/sobel_derivatives.html
 */