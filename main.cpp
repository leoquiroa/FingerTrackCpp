#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cstdlib>
#include <iostream>
#include "Split.h"
#include "Pointer.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv) {    
    if (argc < 2) {
        std::cerr << "Usage:" 
                << argv[0] 
                << " VideoName.mp4" 
                << std::endl;
        return 1;
    }

    Split split;
    split.splitVideo(argv[1]);
    
    Pointer pointer;
    pointer.classifyFrames(split.numberFrame,split.folderName);
    pointer.predictData();
    pointer.calculateTransitions();
    pointer.writeJSON();
    
    return 0;
}

