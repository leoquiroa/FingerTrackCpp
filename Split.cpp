#include "Split.h"

////////////////////////////////////////////////////////////////////////////////

CvCapture* capture = NULL;
IplImage* frame = NULL;
IplImage* grayArray = NULL;
Mat grayImage;
stringstream sss;

////////////////////////////////////////////////////////////////////////////////

int Split::splitVideo(const char* fileName) {
    numberFrame = 1;
    createFolder(fileName);
    capture = cvCreateFileCapture(fileName);  
    if(InvalidVideo(capture)) return 1;
    while(1)
    {
        frame = cvQueryFrame(capture);
        if(InvalidFrame(frame)) break;
        convertToGray();
        if(saveFrame()) break;
        increaseCleanIndex();
        if(IsDone()) break;
    }
    destroyObjects();
    return 0;  
}

////////////////////////////////////////////////////////////////////////////////

void Split::createFolder(const char* fileName){
    char * fileNameCast = strdup(fileName);
    char * pch = strtok (fileNameCast,".");
    folderName = pch;
    system(("mkdir " + folderName).c_str());
}

bool Split::IsDone(){
    char c = cvWaitKey(33);
    if(c == 27)
        return true;
    else
        return false;
}
void Split::increaseCleanIndex(){
    numberFrame++;
    sss.str("");
    sss.clear();
}
string Split::castIndex(){
    sss << numberFrame;
    return sss.str();
}
bool Split::saveFrame(){
    try {
        imwrite(folderName + "/f" + castIndex() + ".png", grayImage);
        return false;
    }
    catch (runtime_error& ex) {
        fprintf(stderr, "Exception saving gray image: %s\n", ex.what());
        return true;
    }
}
void Split::convertToGray(){
    if (!grayArray) // allocate space for the GRAY frame only once
        grayArray = cvCreateImage(cvGetSize(frame), frame->depth,1);
    cvCvtColor(frame, grayArray, CV_RGB2GRAY); // convert RGB frame to GRAY
    grayImage = cv::cvarrToMat(grayArray);
}
bool Split::InvalidFrame(IplImage* fram){
    if (fram) 
        return false;
    else
    {
        fprintf(stderr, "Completed the frame extraction from video file!\n");
        return true;
    }
}
bool Split::InvalidVideo(CvCapture* capt){
    if (capt) 
        return false;
    else
    {  
        fprintf(stderr, "Cannot open video file!\n");
        return true;  
    }
}
void Split::destroyObjects(){
    if (capture)
        cvReleaseCapture(&capture);  
    if (frame)
        cvReleaseImage(&frame);
    if (grayArray)
        cvReleaseImage(&grayArray);
    grayImage.release();
}