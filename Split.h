#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class Split
{
    public:
        string folderName;
        int numberFrame;
        int splitVideo(const char* fileName);
    private:
        void createFolder(const char* fileName);
        void destroyObjects();
        bool InvalidVideo(CvCapture* capt);
        bool InvalidFrame(IplImage* fram);
        void convertToGray();
        bool saveFrame();
        string castIndex();
        void increaseCleanIndex();
        bool IsDone();
};
