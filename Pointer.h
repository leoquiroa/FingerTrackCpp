#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class Pointer
{
    public:
        int classifyFrames(int totalFrames,string folderName);
        int predictData();
        int calculateTransitions();
        void writeJSON();
        void printValues();
    private:
        template<class Type1>
        string castValue(Type1 value);
        Mat readImg(int id);
        Mat getDiffImg(Mat A, Mat B);
        vector<Vec3f> getCircles(Mat img);
        bool IsOnlyFinger(Mat img, float thd);
        void ClassifyPointer(vector<Vec3f> c, int i);
        void setValuePointer(int i, vector<Vec3f> c, int ix);
        ////////////////////////////////////////////////////////////////////////
        float avgRadio();
        float calculateStepForP(int ix, int sizeJump, char coord);
        void calculateStepForT(vector<int> ListToFix);
        void fillHoles(vector<int> ListToFix, float stepX, float stepY, float meanR);
        void whenBeginAndEnd(vector<int> ListToFix, int ix, float meanR);
        void whenOnlyBegin(vector<int> ListToFix, float meanR);
        ////////////////////////////////////////////////////////////////////////
        vector<Vec3f> applySobel(Mat cropImg);
        Mat cropImg(int ix, float xCurrent, float yCurrent);
        int valCropBgn(int value, int limit);
        int valCropEnd(int value, int limit, int rangeCrop);
        void replaceTransitions(int ix, float xCurrent, float yCurrent, vector<Vec3f> circles);
};
